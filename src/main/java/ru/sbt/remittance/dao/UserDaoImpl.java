package ru.sbt.remittance.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sbt.remittance.model.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class UserDaoImpl implements UserDao {
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    public UserDaoImpl(NamedParameterJdbcTemplate jdbcTemplate, PasswordEncoder passwordEncoder) {
        this.jdbcTemplate = jdbcTemplate;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User create(String login, String password, String firstName, String lastName) throws DuplicateUsernameException {
        Map<String, Object> params = new HashMap<>();
        params.put("login", login);

        int numOfUserWithLogin = jdbcTemplate.queryForObject("SELECT count(*) AS countUser FROM Users WHERE login = :login ", params, int.class);

        if (numOfUserWithLogin == 0) {

            String passwordHash = passwordEncoder.encode(password);

            params.put("passwordHash", passwordHash);
            params.put("firstName", firstName);
            params.put("lastName", lastName);

            KeyHolder holder = new GeneratedKeyHolder();

            jdbcTemplate.update("INSERT INTO Users(login, password_Hash, first_Name, last_Name)" +
                    " VALUES (:login, :passwordHash, :firstName, :lastName)", new MapSqlParameterSource(params), holder);

            return new User(holder.getKey().longValue(), login, passwordHash, firstName, lastName);
        }

        throw new DuplicateUsernameException();
    }

    @Override
    public User findByUsername(String login) {
        Map<String, Object> params = new HashMap<>();
        params.put("login", login);
        return jdbcTemplate.queryForObject("SELECT TOP 1 * FROM Users WHERE login = :login", params,
                userRowMapper());
    }

    @Override
    public List<User> getAll() {
        return jdbcTemplate.query("SELECT * FROM Users", userRowMapper());
    }

    private RowMapper<User> userRowMapper() {
        return (rs, i) -> new User(rs.getLong("id"), rs.getString("login"), rs.getString("password_Hash"),
                rs.getString("first_Name"), rs.getString("last_Name"));
    }
}
