package ru.sbt.remittance.services;

import ru.sbt.remittance.model.User;

public interface UserService {
    User getCurrentUser();
    void authenticate(String login, String password);
}
