package ru.sbt.remittance.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.sbt.remittance.dao.UserDao;
import ru.sbt.remittance.model.User;

import java.util.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);

        if (user == null) throw new UsernameNotFoundException("Пользователь не найден");

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPasswordHash(),
                Arrays.asList(new SimpleGrantedAuthority("User")));
    }
}
