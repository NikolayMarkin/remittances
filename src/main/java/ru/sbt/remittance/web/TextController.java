package ru.sbt.remittance.web;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.sbt.remittance.model.Text;
import ru.sbt.remittance.repository.TextRepository;
import ru.sbt.remittance.services.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/texts")
public class TextController {
    private final TextRepository textRepository;
    private final UserService userService;

    public TextController(TextRepository textRepository, UserService userService) {
        this.textRepository = textRepository;
        this.userService = userService;
    }

    @GetMapping
    public ModelAndView list() {
        Iterable<Text> texts = this.textRepository.findAll();
        return new ModelAndView("texts/list", "texts", texts);
    }

    @GetMapping("{id}")
    public ModelAndView view(@PathVariable("id") Text text) {
        return new ModelAndView("texts/view", "text", text);
    }

    @GetMapping(params = "form")
    public String createForm(@ModelAttribute Text text) {
        return "texts/form";
    }

    @PostMapping
    public ModelAndView create(@Valid Text text, BindingResult result,
                               RedirectAttributes redirect) {
        if (result.hasErrors()) {
            return new ModelAndView("texts/form", "formErrors", result.getAllErrors());
        }
        text.setUser(userService.getCurrentUser());
        text = this.textRepository.save(text);
        redirect.addFlashAttribute("globalMessage", "Новый текст успешно сохранен");
        return new ModelAndView("redirect:/texts/{text.id}", "text.id", text.getId());
    }

    @GetMapping(value = "delete/{id}")
    public ModelAndView delete(@PathVariable("id") Long id) {
        this.textRepository.delete(id);
        Iterable<Text> texts = this.textRepository.findAll();
        return new ModelAndView("texts/list", "texts", texts);
    }

    @GetMapping(value = "modify/{id}")
    public ModelAndView modifyForm(@PathVariable("id") Text text) {
        return new ModelAndView("texts/form", "text", text);
    }
}
