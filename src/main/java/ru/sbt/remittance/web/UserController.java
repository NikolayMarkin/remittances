package ru.sbt.remittance.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sbt.remittance.dao.DuplicateUsernameException;
import ru.sbt.remittance.dao.UserDao;
import ru.sbt.remittance.model.User;
import ru.sbt.remittance.repository.TextRepository;
import ru.sbt.remittance.repository.UserRepository;
import ru.sbt.remittance.services.UserService;

import java.util.Date;

@Controller
public class UserController {
    private final UserDao userDao;
    private final UserService userService;
    private final UserRepository userRepository;
    private final TextRepository textRepository;

    public UserController(UserDao userDao, UserService userService, UserRepository userRepository, TextRepository textRepository) {
        this.userDao = userDao;
        this.userService = userService;
        this.userRepository = userRepository;
        this.textRepository = textRepository;
    }

    @RequestMapping("/")
    public ModelAndView home() {
        if (userService.getCurrentUser() != null) {
            return new ModelAndView("redirect:/cards");
        }
        return new ModelAndView("index");
    }

    @ResponseBody
    @RequestMapping(value = "/now")
    public String hello() {
        return "Now " + new Date();
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registerNewUser() {
        return "users/createOrUpdateUserForm";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView registerNewUser(@RequestParam String firstName,
                                        @RequestParam String lastName,
                                        @RequestParam String login,
                                        @RequestParam String password) {

        try {
            User newUser = userDao.create(login, password, firstName, lastName);
        } catch (DuplicateUsernameException e) {
            return new ModelAndView("users/createOrUpdateUserForm", "error", "Пользователь с таким логином уже существует");
        }

        userService.authenticate(login, password);

        return new ModelAndView("redirect:/cards");
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public ModelAndView list() {
        Iterable<User> users = this.userDao.getAll();
        return new ModelAndView("users/list", "users", users);
    }

    @GetMapping("/users/{id}")
    public ModelAndView viewProfile(@PathVariable("id") User user) {
        return new ModelAndView("users/profile", "user", user)
                .addObject("texts", textRepository.findByUserId(user.getId()));
    }
}
