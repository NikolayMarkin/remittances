package ru.sbt.remittance.model;

import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String login;
    private String passwordHash;
    private String firstName;
    private String lastName;

    protected User() {
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public User(long id, String login, String passwordHash, String firstName, String lastName) {
        this.id = id;
        this.login = login;
        this.passwordHash = passwordHash;

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", passwordHash=" + passwordHash +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
