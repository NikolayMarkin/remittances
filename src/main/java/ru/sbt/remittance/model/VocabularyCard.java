package ru.sbt.remittance.model;

import javax.persistence.*;

@Entity
@Table(name = "CARDS")
public class VocabularyCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String word;

    private String translation;

    private boolean minded;

    public VocabularyCard(String word, String translation, boolean minded, User user) {
        this.word = word;
        this.translation = translation;
        this.minded = minded;
        this.user = user;
    }

    protected VocabularyCard() {
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    private User user;

    public long getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getTranslation() {
        return translation;
    }

    public boolean isMinded() {
        return minded;
    }

    public User getUser() {
        return user;
    }

    public void setId(long id) {

        this.id = id;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public void setMinded(boolean minded) {
        this.minded = minded;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
