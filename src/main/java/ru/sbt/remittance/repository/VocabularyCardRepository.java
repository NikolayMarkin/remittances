package ru.sbt.remittance.repository;

import org.springframework.data.repository.CrudRepository;
import ru.sbt.remittance.model.VocabularyCard;

public interface VocabularyCardRepository extends CrudRepository<VocabularyCard, Long> {
    Iterable<VocabularyCard> findByUserId(Long userId);

    VocabularyCard findOneByUserIdAndWord(Long userId, String word);
}
