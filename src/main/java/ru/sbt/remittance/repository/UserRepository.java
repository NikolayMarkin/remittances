package ru.sbt.remittance.repository;

import org.springframework.data.repository.CrudRepository;
import ru.sbt.remittance.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
}
