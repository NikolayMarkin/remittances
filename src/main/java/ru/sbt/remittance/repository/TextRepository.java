package ru.sbt.remittance.repository;

import org.springframework.data.repository.CrudRepository;
import ru.sbt.remittance.model.Text;

public interface TextRepository extends CrudRepository<Text, Long> {
    Iterable<Text> findByUserId(Long userId);
}
